<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use http\Env\Response;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    /**
     * @OA\Get(
     *     path="/hello",
     *     tags={"helloworld"},
     *     summary="Returns a Sample API Hello World",
     *     description="Returns a Sample API Hello World",
     *     operationId="helloworld",
     *     @OA\Response(
     *         response="200",
     *         description="Success Get Hello Wolrd"
     *     )
     * )
     */
    public function Onehello()
    {
        $json['STATUS']    = 200;
        $json['MESSAGE']   = "Success Get Hello Wolrd";
        $json['DATA']      = "Hello World";
        return response($json);
    }
    /**
     * @OA\Post(
     * path="/SamplePost",
     * summary="Sample Post API",
     * description="Post by email, password",
     * operationId="SamplePost",
     * tags={"auth"},
     *   @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=200,
     *       description="Ok",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *       description="Missing data"
     *   )
     */
    public function OneSamplePost(Request $request)
    {
        if (empty($request['email']) && empty($request['password'])) {
            return new \Exception('Missing data', 400);
        }
        $json['STATUS']    = 200;
        $json['MESSAGE']   = "Ok";
        $json['DATA']      = $request->all();
        return response($json);
    }
}
